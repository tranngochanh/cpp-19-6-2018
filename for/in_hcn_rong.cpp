#include <iostream>

using namespace std;

int main()
{
	int a,b;
	
	cout << "Nhap vao chieu dai hinh chu nhat: ";
	cin >> a;
	cout << "Nhap vao chieu rong hinh chu nhat: ";
	cin >> b;
	
	for (int i = 1; i <= b; i++)
	{
		if (i == 1 || i == b)
			for (int j = 1; j <= a; j++)
				cout << "*";
		else 
		{
			cout << "*";
			for (int j = 1; j <= a-2; j++)
				cout << " ";
			cout << "*";
		}
					
		cout << endl;
	}
	
	return 0;
}
