#include <iostream>

using namespace std;

int main()
{
	int n;
	
	cout << "Nhap vao chieu cao cua tam giac: ";
	cin >> n;
	
	for (int i = 1; i <= n; i++) 
	{
		if (i != n)
		{
			for (int j = 1; j <= n - i; j++)
				cout << " ";
			for (int k = 1; k <= 2*i - 1; k++)
			{
				if (k == 1 || k == 2*i - 1) 
					cout << "*";
				else
					cout << " ";
			}
		}
		else 
		{
			for (int j = 1; j <= 2*i - 1; j++)
				cout << "*";
		}
		cout << endl;
	}
	
	return 0;
}
