#include <iostream>

using namespace std;

int main()
{
	int mang[3][4] = 
	{
		{ 1, 3, 5, 1 },
		{ 2, 4, 5, 6 },
		{ 2, 9, 0, 4 }
	};
	
//	cout << mang[0][3];
	
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << i << "-" << j << ".";
			cin >> mang[i][j];
		}
	}
	
	cout << "Ket qua nhap: " << endl;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << mang[i][j] << " ";
		}
		cout << endl;
	}
	
	return 0;
}
