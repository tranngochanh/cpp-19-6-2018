#include <iostream>

using namespace std;

int main()
{
	int n, *mang, *mangDaoNguoc;
	
	cout << "Nhap vao so luong phan tu cua mang: ";
	cin >> n;
	
	mang = new int[n];
	mangDaoNguoc = new int[n];
	
	cout << "Nhap vao cac phan tu trong mang: " << endl;
	for (int i = 0; i < n ; i++)
	{
		cout << i << ". ";
		cin >> mang[i];
	}
	
	for (int i = 0, j = n-1; i < n; i++, j--)
	{
		mangDaoNguoc[i] = mang[j];
	}
	
	cout << "Mang sau khi dao nguoc: ";
	
	for (int i = 0; i < n; i++)
	{
		cout << mangDaoNguoc[i] << " ";
	}
	
	return 0;
}
