# include <iostream>
using namespace std;

bool kiemTraSNT(int a);

int main ()
{
	int n;
	cout <<"Nhap tong so phan tu trong mang: ";
	cin >> n;
	int mang [n];
	
	cout <<"Nhap tung phan tu trong mang: " << endl;
	for (int i=0;i<n;i++)
	{
		cout <<i<<". ";
		cin >> mang [i];
	}
	
	for (int i = 0; i < n; i++)
	{
		if (kiemTraSNT(mang[i]))
		{
			for (int j = i + 1; j < n; j++)
			{
				if (mang[i] > mang[j] && kiemTraSNT(mang[j]))
				{
					int temp = mang[i];
					mang[i] = mang[j];
					mang[j] = temp;
				}
			}
		}
	}
	
	cout << "Mang da sap xep tang dan: " << endl;
	for (int i = 0; i < n; i++)
		cout << mang[i] << " ";
	
	return 0;
}

bool kiemTraSNT(int a)
{
	if (a < 2)
		return false;
		
	for (int i = 2; i < a; i++)
	{
		if (a % i == 0)
			return false;
	}
	return true;
}
