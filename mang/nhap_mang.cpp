#include <iostream>

using namespace std;

int main()
{
	int n;
	
	cout << "Nhap so luong phan tu cua mang: ";
	cin >> n;
	
	int mang[n];
	
	cout << "Nhap vao gia tri cac phan tu: "<< endl;
	for (int i = 0; i < n; i++)
	{
		cout << i << ". ";
		cin >> mang[i];
	}
	
	cout << "Ket qua mang da nhap: ";
	for (int i = 0; i < n; i++)
		cout << mang[i] << " ";
	
	return 0;
}
