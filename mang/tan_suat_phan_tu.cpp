#include <iostream>
#include <string>

using namespace std;

int main()
{
	int n,dem, *mang, mark;
	
	cout << "Nhap vao so luong phan tu cua mang: ";
	cin >> n;
	
	mang = new int[n];
	
	cout << "Nhap vao cac phan tu trong mang: " << endl;
	for (int i = 0; i < n ; i++)
	{
		cout << i << ". ";
		cin >> mang[i];
	}
	
	mark = mang[0];
	for(int i=0;i<n;i++)
	{
		dem = 1;
		if (i == 0 || mang[i] != mark)
		{
			for(int j=i+1;j<n;j++)
			{
				if(mang[i]==mang[j])
				{
					dem++;
					mang[j] = mark;
				}					
			}
			cout<<"so "<<mang[i]<<" xuat hien "<<dem<<" lan" << endl;
		}	
	}
	return 0;
}
