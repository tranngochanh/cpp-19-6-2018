#include <iostream>

using namespace std;

int main()
{
	int height[] = {1,2,3,4,5,6};
	
	for (int i = 0; i < 6; i++) 
	{
		height[i] = i;
	}
	
	cout << "Ket qua mang: " << endl;
	for (int i = 0; i < 6; i++)
		cout << height[i];
	
	return 0;
}
