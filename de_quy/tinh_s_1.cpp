/*
S(n) = 1 + 1/3 + 1/5 + ... + 1/(2n+1)
*/
#include <iostream>

using namespace std;

float tinhS(int n) 
{
	if (n == 0)
		return 1;
	else
		return (float) 1/(2*n + 1) + tinhS(n-1);
}

int main()
{
	int n;
	
	cout << "Nhap vao so can tinh: ";
	cin >> n;
	cout << "Ket qua: " << tinhS(n);
	
	return 0;
}
