/* B�i 5: T�nh S(n) = 1 + 1/3 + 1/5 + ... + 1/(2n + 1) */
#include <iostream>
using namespace std;
float tinhS(int n);
int main()
{
	int n;
	cout<<"Nhap n: ";
	cin>>n;
	cout<<"Ket qua: "<<tinhS(n);
	return 0;
}
float tinhS(int n)
{
	return (n==0)? 1: tinhS(n-1)+ (float)1/(2*n+1);
}

