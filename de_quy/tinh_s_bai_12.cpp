/* B�i 12: T�nh S(n) = x + x^2 + x^3 + ... + x^n */
#include <iostream>

using namespace std;

long tinhS(int x, int n);
int power (int x, int n);

int main()
{
	int x, n;
	cout << "Nhap x: ";
	cin >> x;
	cout << "Nhap n: ";
	cin >> n;
	cout << "Ket qua: " << tinhS(x, n);
	return 0;
}
long tinhS(int x, int n)
{
	return (n==1) ? x : tinhS(x, n-1) + power(x,n);
}
int power (int x, int n)
{
	return (n==1) ? x : power(x, n-1) * x;
}
