#include <iostream>

using namespace std;

int power(int x, int n);

int main()
{
	int n,x;
	cout << "Nhap vao n: ";
	cin >> n;
	cout << "Nhap vao x: ";
	cin >> x;
	
	cout << "Ket qua x^n: " << power(x, n);
	
	return 0;
}

int power(int x, int n)
{
	if (n == 1)	
		return x;
	else
		return power(x, n - 1) * x;
}

