/* B�i 11: T�nh S(n) = 1 + 1.2 + 1.2.3 + ... + 1.2.3....N*/
#include <iostream>

using namespace std;

long giaiThua(int n);
long tongGiaiThua(int n);

int main()
{
	int n;
	cout << "Nhap vao so n: ";
	cin >> n;
	
	cout << "Tong giai thua: " << tongGiaiThua(n);
	return 0;
}

long giaiThua(int n)
{
	if (n == 1)
		return 1;
	else
		return giaiThua(n-1) * n;
}

long tongGiaiThua(int n)
{
	if (n == 1)
		return 1;
	else
		return tongGiaiThua(n-1) + giaiThua(n);
}
