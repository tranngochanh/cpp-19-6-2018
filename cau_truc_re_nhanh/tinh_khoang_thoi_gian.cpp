/*
Nhap gio, phut, giay bat dau
Nhap gio, phut, giay ket thuc
Tinh khoang thoi gian troi qua theo giay ?
*/
#include <iostream>

using namespace std;

int main()
{
	int gio1, phut1, giay1, thoiGian1;
	int gio2, phut2, giay2, thoiGian2;
	int khoangThoiGian;
	
	do 
	{
		cout<<" nhap vao gio: ";
		cin>>gio1;
		cout<<" nhap vao so phut : ";
		cin>>phut1;
		cout<<" nhap vao so giay : ";
		cin>>giay1;
	} while (gio1>24||phut1>60||giay1>60);
	
	thoiGian1 = gio1*3600 + phut1*60 + giay1;
	
	do 
	{
		cout<<" nhap vao gio: ";
		cin>>gio2;
		cout<<" nhap vao so phut : ";
		cin>>phut2;
		cout<<" nhap vao so giay : ";
		cin>>giay2;
	} while (gio2>24||phut2>60||giay2>60);
	
	thoiGian2 = gio2*3600 + phut2*60 + giay2;
	
	khoangThoiGian = thoiGian2 - thoiGian1;
	if (khoangThoiGian < 0)
		khoangThoiGian *= -1;
		
	cout << " Khoang cach giua 2 thoi diem la: " << khoangThoiGian << " giay";
	
	return 0;
}
