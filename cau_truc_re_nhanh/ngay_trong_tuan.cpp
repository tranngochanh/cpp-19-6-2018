#include <iostream>

using namespace std;

int main()
{
	int n;
	cout << "Nhap vao 1 so tu 1 den 7: ";
	cin >> n;
	
	if (n > 7 || n < 1) 
	{
		cout << "Ban nhap khong chinh xac";
	}
	else 
	{
		if (n == 1)
			cout << "Thu hai";
		else if (n == 2)
			cout << "Thu ba";
		else if (n == 3)
			cout << "Thu tu";
		else if (n == 4)
			cout << "Thu nam";
		else if (n == 5)
			cout << "Thu sau";
		else if (n == 6)
			cout << "Thu bay";
		else 
			cout << "Chu nhat";
	}
	
	return 0;
}
