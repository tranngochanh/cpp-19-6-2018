/*
Nhap gio, phut, giay dung (khong dung phai nhap lai)
Sau do nhap 1 so giay
=> In ra ket qua thoi gian + so giay

Vi du:
Nhap thoi gian: 8:45:30
So giay: 1235
=> Ket qua: 09:06:05
*/
#include <iostream>
#include <string>

using namespace std;

int main()
{
	long h,ph,gi,githem;
	/* Kiem tra nhap gio, phut, giay dung hay ko ? */
	do 
	{
		cout<<" nhap vao gio: ";
		cin>>h;
		cout<<" nhap vao so phut : ";
		cin>>ph;
		cout<<" nhap vao so giay : ";
		cin>>gi;
	} while (h>24||ph>60||gi>60);

	cout<<" nhap vao so giay them :";
	cin>>githem;
	long tong=h*3600+ph*60+gi+githem;
	h=tong/3600;
	ph=(tong%3600)/60;
	gi=tong-h*3600-ph*60;
	
	string so0ph = ph < 10 ? "0" : "";
	string so0gi = gi < 10 ? "0" : "";
	cout<< " thoi gian la : "<<h<<":"<< so0ph << ph <<":"<< so0gi << gi;
	
	return 0;
}
