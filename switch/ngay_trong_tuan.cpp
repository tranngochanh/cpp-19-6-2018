#include <iostream>

using namespace std;

int main()
{
	int n;
	
	cout << "Nhap vao 1 so tu 1 den 7: ";
	cin >> n;
	
	if (n > 7 || n < 1) 
	{
		cout << "Ban nhap khong chinh xac";
	}
	else 
	{
		switch(n)
		{
			case 1:
				cout << "Thu hai";
				break;
			case 2:
				cout << "Thu ba";
				break;
			case 3:
				cout << "Thu tu";
				break;
			case 4:
				cout << "Thu nam";
				break;
			case 5:
				cout << "Thu sau";
				break;
			case 6:
				cout << "Thu bay";
				break;
			default:
				cout << "Chu nhat";
		}
	}
	
	return 0;
}
