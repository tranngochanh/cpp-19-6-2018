#include <iostream>

using namespace std;

int main() 
{
	int a, b;
	
	cout << "Ban hay nhap vao so thu nhat: ";
	cin >> a;
	cout << "Ban hay nhap vao so thu hai: ";
	cin >> b;
	
	cout << "Cong 2 so: " << a + b << endl;
	cout << "Tru 2 so: " << a - b << endl;
	cout << "Nhan 2 so: " << a * b << endl;
	cout << "Chia 2 so: " << a / b << endl;
	
	return 0;
}
