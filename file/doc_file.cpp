#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	int so;
	string data;
	
	ofstream ouf("./data/output.txt", ios::app);
	ouf << "Hello world" << endl;
	ouf.close();
	
	ifstream inf("./data/input.txt", ios::in);
	inf >> so;
	cout << "so: " << so << endl;
	
	getline(inf, data);
	cout << "chuoi 1: " << data << endl;
	getline(inf, data);
	cout << "chuoi 2: " << data << endl;
	getline(inf, data);
	cout << "chuoi 3: " << data << endl;
	
	inf.close();
	
	return 0;
}
