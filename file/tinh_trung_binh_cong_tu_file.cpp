#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	float tb;
	int dem;
	
	ifstream inf("./data/tb_input.txt", ios::in);

	if (inf.bad())
		cout << "File khong ton tai";
	else 
	{
		while (!inf.eof()) 
		{
			int a;
			inf >> a;
			tb += a;
			dem++;
		}

	}		
	
	inf.close();
	
	cout << "Trung binh cong: " << tb / dem;
	
	return 0;
}
