#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <conio.h>

using namespace std;

int main()
{
	char ch;
	
	fstream fnguon("./data/hoidong.jpg", ios::in | ios::binary);
	fstream fdich("./data/hoidong_copy.jpg", ios::out | ios::binary);

	while (!fnguon.eof()) {
		fnguon.get(ch);
		fdich.put(ch);
	}
	
	fnguon.close();
	fdich.close();
	
	return 0;
}

