#include <iostream>

using namespace std;

int main()
{
	int n, Max, m;
	cout << "Nhap vao so n: ";
	cin >> n;
	m = n;
	
	do 
	{
		int i = n % 10;
		n = n / 10;
		if (Max < i)
			Max = i;
	} while (n != 0);
	
	cout << "Chu so lon nhat cua " << m << " la " << Max;
	
	return 0;
}
