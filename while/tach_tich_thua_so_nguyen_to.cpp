#include <iostream>

using namespace std;

int main()
{
	int n, i=2;
	
	cout << "Nhap vao 1 so: ";
	cin >> n;
	
	cout << "Tich thua so nguyen to cua " << n << " la: ";
	while (n!=1) 
	{
		if (n % i == 0)
		{
			cout << i <<  " ";
			n = n / i;
		} 
		else 
			i++;
	}
	
	return 0;
}
