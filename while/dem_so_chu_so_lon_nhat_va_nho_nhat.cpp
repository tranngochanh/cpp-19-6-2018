#include <iostream>

using namespace std;

int main()
{
	int max = 0, min = 10, n, demMax = 0, demMin = 0;
	
	cout << "Nhap vao so n: ";
	cin >> n;
	
	while (n != 0)
	{
		int i = n % 10;
		n = n / 10;
		
		if (max < i) 
		{
			max = i;
			demMax = 1;
		} 
		else if (max == i) 
			demMax++;
		
		if (min > i)
		{
			min = i;
			demMin = 1;
		}
		else if (min == i)
			demMin++;
	}
	
	cout << "So chu so lon nhat trong n la: " << max << " - " << demMax << endl;
	cout << "So chu so nho nhat trong n la: " << min << " - " << demMin << endl;
	
	return 0;
}
