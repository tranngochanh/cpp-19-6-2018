#include <iostream>
#include <string>

using namespace std;

int main()
{
	string chuoi1;
	string chuoi2;
	string chuoi3; 
	
	do {
		cout<<"nhap chuoi thu nhat ";
		getline(cin,chuoi1);
	} while(chuoi1.empty());
	
	do {
		cout<<"nhap chuoi thu hai ";
		getline(cin,chuoi2);
	} while(chuoi2.empty());
	
	do {
		cout<<"nhap chuoi thu ba ";
		getline(cin,chuoi3);
	} while(chuoi3.empty());
	
	int vitri= chuoi1.find(chuoi2);
	if(vitri>=0&&vitri<chuoi1.length())
	{
		chuoi1.replace(vitri,chuoi2.length(),chuoi3);
	}
	cout<<"chuoi sau khi thay la: "<<chuoi1;
	
	return 0;
}
