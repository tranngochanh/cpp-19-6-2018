#include <iostream>
#include <string>

using namespace std;

int main()
{
	string chuoi;
	string chuoiReverse = "";
	
	cout << "Nhap vao chuoi: ";
	getline(cin, chuoi);
	
	for (int i = chuoi.length() - 1; i >= 0; i--)
	{
		chuoiReverse += chuoi[i];
	}
	
	cout << "Chuoi nghich dao: " << chuoiReverse;
	
	return 0;
}
