#include <iostream>
#include <string>
using namespace std;
int main()
{
	string str, strReverse="";
	cout<<"Nhap mot chuoi: ";
	getline (cin, str);
	
	/* C�ch 1 */
	for (int i= str.length()-1; i>=0; i--)
		strReverse+= str[i];
	if (str.compare(strReverse)==0)
		cout<<"La chuoi doi xung" << endl;
	else
		cout<<"Khong la chuoi doi xung" << endl;
		
	/* C�ch 2 */
	int i = 0, j = str.length() - 1;
	while (j > i)
	{
		if (str[i] != str[j])
		{
			cout<<"Khong la chuoi doi xung" << endl;
			break;
		}
		else 
		{
			i++;
			j--;
		}
	}
	if (j <= i)
		cout<<"La chuoi doi xung" << endl;
		
	return 0;
}
