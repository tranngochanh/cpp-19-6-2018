#include <iostream>
#include <string>

using namespace std;

int main()
{
	string chuoi1, chuoi2;
	cout<<"nhap vao chuoi ";
	getline(cin,chuoi1);
	
	for(int i = 0; i < chuoi1.length(); i++)
	{
		int charInt = (int) chuoi1[i];
		if (charInt >= 65 && charInt <= 90)
		{
			charInt += 32;
			chuoi2 += (char) charInt;
		}
		else 
			chuoi2 += chuoi1[i];
	}
	
	cout << "Ket qua: " << chuoi2;
	
	return 0;
}
