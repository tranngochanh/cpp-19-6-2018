#include <iostream>

using namespace std;

void thamTri(int a, int b, int c);
void thamChieu(int &a, int &b, int &c);

int main()
{
	
	int a = 10, b = 20, c = 30;
	
	cout << a << endl << b << endl << c << endl;
	
	thamTri(a,b,c);
	
	cout << "Sau khi goi ham tham tri: " << endl << a << endl << b << endl << c << endl;
	
	thamChieu(a,b,c);
	
	cout << "Sau khi goi ham tham chieu: " << endl << a << endl << b << endl << c << endl;
	
	return 0;
}

void thamTri(int a, int b, int c)
{
	a = a * 10;
	b = b / 2;
	c = c + 7;
}

void thamChieu(int &a, int &b, int &c)
{
	a = a * 10;
	b = b / 2;
	c = c + 7;
}
