#include <iostream>

using namespace std;

float mayTinh(int so1, int so2, int x);
void inChuongTrinh();

int main()
{
	int a, b, phepTinh;
	
	cout << "Nhap vao so a: ";
	cin >> a;
	cout << "Nhap vao so b: ";
	cin >> b;
	
	inChuongTrinh();
	cin >> phepTinh;
	
	while (phepTinh < 0 || phepTinh > 4) 
	{
		inChuongTrinh();
		cin >> phepTinh;
	}
	
	cout << "Ket qua la: " << mayTinh(a, b, phepTinh);
	
	return 0;
}

float mayTinh(int so1, int so2, int x) 
{
	switch (x)	
	{
		case 0:
			return so1 + so2;
		case 1:
			return so1 - so2;
		case 2:
			return so1 * so2;
		case 3:
			return (float) so1 / so2;
		case 4: 
			return so1 % so2;
	}
}

void inChuongTrinh()
{
	cout << "Nhap vao phep tinh tuong ung: " << endl;
	cout << "0 la phep cong" << endl;
	cout << "1 la phep tru" << endl;
	cout << "2 la phep nhan" << endl;
	cout << "3 la phep chia" << endl;
	cout << "4 la php chia lay du" << endl;
}
