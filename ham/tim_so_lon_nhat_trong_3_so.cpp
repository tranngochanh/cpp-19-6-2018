#include <iostream>

using namespace std;

int timSoLonNhat(int a, int b, int c);

int main()
{
	int a, b, c;
	
	cout << "Nhap vao so thu nhat: ";
	cin >> a;
	cout << "Nhap vao so thu hai: ";
	cin >> b;
	cout << "Nhap vao so thu ba: ";
	cin >> c;
	
	cout << "So lon nhat la: " << timSoLonNhat(a, b, c);
	return 0;
}

int timSoLonNhat(int a, int b, int c)
{
	int max = a;
	if (max < b)
		max = b;
	if (max < c)
		max = c;
	return max;
}
